﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        // Global blockchain object
        private Blockchain blockchain;
        public static bool difficulty_check;
        public List<long> blocktime;
        public static double timeAverage;
        public static double currentSum;
        public static long firstTime;
        public static int difficulty = 3;
        

        // Default App Constructor
        public BlockchainApp()
        {
            // Initialise UI Components
            InitializeComponent();
            // Create a new blockchain 
            blockchain = new Blockchain();
            // Update UI with an initalisation message
            UpdateText("New blockchain initialised!");
        }

        

        public static bool getDifficultyCheck()
        {
            return difficulty_check;
        }

        /* PRINTING */
        // Helper method to update the UI with a provided message
        private void UpdateText(String text)
        {
            output.Text = text;
        }

        // Print entire blockchain to UI
        private void ReadAll_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.ToString());
        }

        // Print Block N (based on user input)
        private void PrintBlock_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(blockNo.Text, out int index))
                UpdateText(blockchain.GetBlockAsString(index));
            else
                UpdateText("Invalid Block No.");
        }

        // Print pending transactions from the transaction pool to the UI
        private void PrintPendingTransactions_Click(object sender, EventArgs e)
        {
            blockchain.transactionPool.Sort((y, x) => x.timestamp.CompareTo(y.timestamp)); // printing the latest transaction at the top
            UpdateText(String.Join("\n", blockchain.transactionPool)); // printing transactions
            if(Greedy.Checked == true && blockchain.transactionPool.Count != 0) // chekcing if radiobutton is active
            {

                blockchain.transactionPool.Sort((y,x) => x.fee.CompareTo(y.fee)); // sorting the list with highest fee's first
 
                UpdateText(String.Join("\n", blockchain.transactionPool));// printing transactions
            }
            else if(Althrustic.Checked == true && blockchain.transactionPool.Count != 0)// chekcing if radiobutton is active
            {
                
                blockchain.transactionPool.Sort((x, y) => x.timestamp.CompareTo(y.timestamp)); // printing the oldest transaction at the very top

                UpdateText(String.Join("\n", blockchain.transactionPool));// printing transactions
            }
            else if(Unpredictable.Checked == true && blockchain.transactionPool.Count != 0)// chekcing if radiobutton is active
            {
                Random rnd = new Random();
                int i = blockchain.transactionPool.Count;
                while(i > 1)
                {
                    // randomising the entire order of the list
                    i--;
                    int k = rnd.Next(i + 1);
                    Transaction value = blockchain.transactionPool[k];
                    blockchain.transactionPool[k] = blockchain.transactionPool[i]; // switching the given indexes of the list
                    blockchain.transactionPool[i] = value;

                }
                
                UpdateText(String.Join("\n", blockchain.transactionPool));// printing transactions
            }
            else if(Address_based.Checked == true && blockchain.transactionPool.Count != 0)// chekcing if radiobutton is active
            {
                List<Transaction> list1 = new List<Transaction>();
                foreach (Transaction transaction in blockchain.transactionPool)
                {
                    // public List<Transaction> transactionPool = new List<Transaction>();
                    String address = Personal_address_textbox.Text;
                    
                    if ( address.Equals(transaction.senderAddress))
                    {
                        list1.Add(transaction);
                        
                    }
                    
                }
                UpdateText(String.Join("\n", list1));// printing transactions from new list
            }
        }

        /* WALLETS */
        // Generate a new Wallet and fill the public and private key fields of the UI
        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out string privKey);

            publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
        }

        // Validate the keys loaded in the UI by comparing their mathematical relationship
        private void ValidateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
                UpdateText("Keys are valid");
            else
                UpdateText("Keys are invalid");
        }

        // Check the balance of current user
        private void CheckBalance_Click(object sender, EventArgs e)
        {
            UpdateText(blockchain.GetBalance(publicKey.Text).ToString() + " Assignment Coin");
        }


        /* TRANSACTION MANAGEMENT */
        // Create a new pending transaction and add it to the transaction pool
        private void CreateTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, reciever.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
            /* TODO: Validate transaction */
            blockchain.transactionPool.Add(transaction);
            UpdateText(transaction.ToString());
        }

        /* BLOCK MANAGEMENT */
        // Conduct Proof-of-work in order to mine transactions from the pool and submit a new block to the Blockchain
        private void NewBlock_Click(object sender, EventArgs e)
        {
            currentSum = 0;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            // Retrieve pending transactions to be added to the newly generated Block
            List<Transaction> transactions = blockchain.GetPendingTransactions();
            if(difficulty_check == true)
            {
                if (Blockchain.blocktime.Count > 3)
                {
                    firstTime = Blockchain.blocktime[0];
                    foreach (long x in Blockchain.blocktime)
                    {

                        currentSum += x;
                    }
                    timeAverage = currentSum / 4;
                    Console.WriteLine(" ");
                    Console.WriteLine("First Block took " + firstTime + " Milliseconds to create");
                    Console.WriteLine("Average time taken to create block in current difficulty is " + timeAverage + "Milliseconds");
                    Console.WriteLine(" ");
                    Blockchain.blocktime.Clear();

                    if (firstTime > timeAverage * 1.1) // compare block time
                    {
                        difficulty = difficulty - 1;// adjust difficulty

                        if (difficulty <= 2) // make sure difficulty never falls below 2
                        {
                            difficulty = 2;
                        }
                    }
                    if (firstTime < timeAverage * 0.9) // compare block time to increase difficulty
                    {
                        difficulty = difficulty + 1; // adjust difficulty
                    }
                    Console.WriteLine("Difficulty adjusted to " + difficulty);


                }
                // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
                Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
                blockchain.blocks.Add(newBlock);

                stopwatch.Stop();

                UpdateText(blockchain.ToString());



                Blockchain.blocktime.Add(stopwatch.ElapsedMilliseconds);
                Console.WriteLine(" New block Created in {0} " + stopwatch.ElapsedMilliseconds + " milliseconds");
                Console.WriteLine(" ");
                Console.WriteLine("Blocks created in Milliseconds in current difficulty");
                Blockchain.blocktime.ForEach(Console.WriteLine);
            }
            if(difficulty_check == false)
            {
                // Create and append the new block - requires a reference to the previous block, a set of transactions and the miners public address (For the reward to be issued)
                Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
                blockchain.blocks.Add(newBlock);

                stopwatch.Stop();

                UpdateText(blockchain.ToString());


                Console.WriteLine(" New block Created in {0} " + stopwatch.ElapsedMilliseconds + " milliseconds");

                
            }

            
        }


        /* BLOCKCHAIN VALIDATION */
        // Validate the integrity of the state of the Blockchain
        private void Validate_Click(object sender, EventArgs e)
        {
            // CASE: Genesis Block - Check only hash as no transactions are currently present
            if(blockchain.blocks.Count == 1)
            {
                if (!Blockchain.ValidateHash(blockchain.blocks[0])) // Recompute Hash to check validity
                    UpdateText("Blockchain is invalid");
                else
                    UpdateText("Blockchain is valid");
                return;
            }

            for (int i=1; i<blockchain.blocks.Count-1; i++)
            {
                if(
                    blockchain.blocks[i].prevHash != blockchain.blocks[i - 1].hash || // Check hash "chain"
                    !Blockchain.ValidateHash(blockchain.blocks[i]) ||  // Check each blocks hash
                    !Blockchain.ValidateMerkleRoot(blockchain.blocks[i]) // Check transaction integrity using Merkle Root
                )
                {
                    UpdateText("Blockchain is invalid");
                    return;
                }
            }
            UpdateText("Blockchain is valid");
        }

        private void validationLabel_Click(object sender, EventArgs e)
        {

        }

        private void amount_TextChanged(object sender, EventArgs e)
        {

        }

        private void blocksLabel_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void blockNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Adjust_difficulty_CheckedChanged(object sender, EventArgs e)
        {
            if(Adjust_difficulty.Checked== true)
            {
                difficulty_check = true;
            }
        }
    }
}