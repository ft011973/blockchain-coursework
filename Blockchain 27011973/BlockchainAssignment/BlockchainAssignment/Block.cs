﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace BlockchainAssignment
{
    class Block
    {
        /* Block Variables */
        private DateTime timestamp; // Time of creation

        private int index, // Position of the block in the sequence of blocks
        difficulty = BlockchainApp.difficulty; // An arbitrary number of 0's to proceed a hash value

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress;
            

        public List<Transaction> transactionList; // List of transactions in this block
        
        // Proof-of-work
        public long nonce,nonce1 = 0,nonce2 = 1; // Number used once for Proof-of-Work and mining

        private string hash1 = "", hash2 = "";

        private bool thread1Finish = false;
        private bool thread2Finish = false;

        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"
        

        /* Genesis block constructor */
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = MultiMine();
            
        }

        /* New Block constructor */
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress)
        {
            timestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions
            
            hash = MultiMine(); // Conduct PoW to create a hash which meets the given difficulty requirement
            
        }

        /* Hashes the entire Block object */
        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }
        public String CreateHash(long nonce)
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash;
        }
        

        // Create a Hash which satisfies the difficulty level required for PoW
        public String MultiMine()
        {
            Thread thread1 = new Thread(Mine1); // create thread
            Thread thread2 = new Thread(Mine2); // create thread

            thread1.Start();// start thread
            thread2.Start();// start thread


            String re = new string('0', difficulty); // A string for analysing the PoW requirement
            while (thread1.IsAlive == true || thread2.IsAlive == true) 
            {
                Thread.Sleep(1);
            }

            if (hash1.StartsWith(re)== true) // comparing if hash1 matches difficulty string then return hash1 as main hash
            {
                nonce = nonce1;
                return hash1;
            }
            else // else return hash 2 as main hash
            {
                nonce = nonce2;
                return hash2;
            }
        
        }

        public void Mine1()
        {

            String tempHash;
            String re = new string('0', difficulty); // A string for analysing the PoW requirement
            
            Boolean check = false;


            while (check==false) // keep looping under condition is met
            {
                tempHash = CreateHash(nonce1); // create a temp hash
                if (tempHash.StartsWith(re) == true) // checking if temp hash meets difficulty condition 
                {
                    thread1Finish = true;
                    check = true;
                    hash1 = tempHash;
                    return;
                }
                else if (thread2Finish == true) // if thread 2 is finished then put Thread to sleep
                {
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    check = false;
                    nonce1 += 2; // return nonce with new incrementation 
                }
                
            }
            
            Console.WriteLine(hash1);
            return;
        }

        public void Mine2()
        {
            String tempHash;
            String re = new string('0', difficulty); // A string for analysing the PoW requirement
            
            Boolean check = false;


            while (check == false) // keep looping under condition is met
            {
                tempHash = CreateHash(nonce2);// create a temp hash
                if (tempHash.StartsWith(re) == true)// checking if temp hash meets difficulty condition 
                {
                    thread2Finish = true;
                    check = true;
                    hash2 = tempHash;
                    return;
                }
                else if (thread1Finish == true)// if thread 1 is finished then put Thread to sleep
                {
                    Thread.Sleep(1);
                    return;
                }
                else
                {
                    check = false;
                    nonce2 += 2;// return nonce with new incrementation 
                }

            }

            Console.WriteLine(hash2);
            return;
        }
       

        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"
            
            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i=0; i<hashes.Count; i+=2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                +"\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }
    }
}
